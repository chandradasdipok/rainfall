package com.geet.rainfall.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

import com.geet.rainfall.model.Row;

public class FileReader {
	
	
	public boolean createRows(String fileName){
		File inputFile = new File(fileName);
		try {
			Scanner scanner = new Scanner(inputFile);
			List<Row> rows = new ArrayList<Row>();
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				StringTokenizer stringTokenizer = new StringTokenizer(line,",",false);
				int i=0;
				String district ="";
				String year="";
				String month ="";
				List<Integer> days = new ArrayList<Integer>();
				while (stringTokenizer.hasMoreTokens()) {
					String token = stringTokenizer.nextToken();
					if (i==0) {
						district = token;
					}
					else if(i==1){
						year = token;
					}else if(i==2){
						month = token;
					}else {
						if (token.startsWith("*")) {
							days.add(-1);
						}else{
							days.add(new Integer(Integer.parseInt(token)));
						}
					}
					i++;
				}
				Row row = new Row.Builder().district(district).year(Integer.parseInt(year)).month(Integer.parseInt(month)).days(days).build();
				rows.add(row);
				
			}
			
			for (int i = 0; i < rows.size(); i++) {
				System.out.print(rows.get(i).getDistrict()+" "+rows.get(i).getYear()+" "+rows.get(i).getMonth()+" ");
				for (int j = 0; j < rows.get(i).getDays().size(); j++) {
					System.out.print(rows.get(i).getDays().get(j).intValue()+" ");
				}
				System.out.println();
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public static void main(String[] args) {
		new FileReader().createRows("src/com/geet/rainfall/res/rainfall.csv");
	}
}
