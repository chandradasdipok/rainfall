package com.geet.rainfall.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Row implements Serializable{

	private String district = "";
	private int  year =-1;
	private int month = -1;
	private List<Integer>days= new ArrayList<Integer>();
	private Row(){	}
	
	public static class Builder{
		
		private String district = "";
		private int  year =-1;
		private int month = -1;
		private List<Integer>days= new ArrayList<Integer>();
		
		public Builder district(String district){
			this.district = district;
			return this;
		}
		
		public Builder year(int year){
			this.year = year;
			return this;
		}
		
		public Builder month (int month){
			this.month = month;
			return this;
		}
		
		public Builder days(List<Integer>days){
			this.days = days;
			return this;
		}
		
		public Row build(){
			Row row = new Row();
			row.setDistrict(district);
			row.setYear(year);
			row.setMonth(month);
			row.setDays(days);
			return row;
		}
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public List<Integer> getDays() {
		return days;
	}

	public void setDays(List<Integer> days) {
		this.days = days;
	}
}
